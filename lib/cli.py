import os
import shutil
import getpass
import pwd
import json
import urllib.request
from sys import argv
from shutil import copyfile

DOWNLOAD_URL = "http://www.killernetworking.com/support/K1535_Debian/board.bin"
HWROOT = "/lib/firmware/ath10k/QCA6174/"
HW21 = HWROOT + "hw2.1/board.bin"
HW3 = HWROOT + "hw3.0/board.bin"

#stolen from https://unix.stackexchange.com/questions/11470/how-to-get-the-name-of-the-user-that-launched-sudo
def get_user():
    """Try to find the user who called sudo/pkexec."""
    try:
        return os.getlogin()
    except OSError:
        # failed in some ubuntu installations and in systemd services
        pass

    try:
        user = os.environ['USER']
    except KeyError:
        # possibly a systemd service. no sudo was used
        return getpass.getuser()

    if user == 'root':
        try:
            return os.environ['SUDO_USER']
        except KeyError:
            # no sudo was used
            pass

        try:
            pkexec_uid = int(os.environ['PKEXEC_UID'])
            return pwd.getpwuid(pkexec_uid).pw_name
        except KeyError:
            # no pkexec was used
            pass

    return user

class Config:
    def __init__(self, config_dir=None, dictconfig=None):
        if not config_dir:
            user = get_user()
            if user:
                config_dir = "/home/{}/.config/".format(user)

        else:
            config_dir = config_dir if config_dir[-1] == "/" else config_dir+"/"

        print(config_dir)
        try:
            conf = json.loads(open(config_dir+"linux-surface-wifi-fix.json").read())
        except FileNotFoundError:
            print("Config not found, using default values...")
            conf = {}

        if dictconfig:
            conf = dictconfig

        self.download_dir = conf.get("download_dir") or "/tmp/linux-surface-wifi-fix/"
        self.download_on_run = conf.get("download_on_run")
        if not self.download_on_run and self.download_on_run is None:
            self.download_on_run = True

        self.save_old_files = conf.get("save_old_files") or False
        self.save_dir = conf.get("save_dir")

        if self.save_dir and self.save_dir[-1] != "!":
            self.save_dir = self.save_dir + "/"
        
        if self.download_dir[-1] != "/":
            self.download_dir = self.download_dir + "/"

        if self.save_dir and not self.save_old_files:
            print("INFO: save_dir set but save_old_file is False")

        

def main():
    args = argv[1:]
    config_args = {"config-dir":"", "save-dir":"", "save-old-files":False, "download-on-run":True, "download-dir": ""}

    current = ""
    for arg in args:
        split_equal = arg.split('=')
        if split_equal[0] not in config_args.keys() and not current:
            print("Argument {} not recognized, valid args are 'config-dir', 'save-dir, 'save-old-files', 'download-on-run', 'download-dir'".format(split_equal[0]))
            exit(0)

        if len(split_equal) == 1 and not current:
            current = arg
        
        elif current and len(split_equal) == 1:
            config_args[current] = arg
            current = ""

        elif len(split_equal) == 2:
            config_args[split_equal[0]] = split_equal[1]

    for k in ("save-old-files", "download-on-run"):
        val = config_args[k]
        if type(val) == str:
            if val in ['True', '1', 'true', 'yes', 'y']:
                config_args[k] = True
            else:
                config_args[k] = False

    config_dir = config_args.get('config')
    if not config_dir:
        c = {}
        for k,v in config_args.items():
            c[k.replace('-', '_')] = v
        
        conf = Config(dictconfig=c)

    else:
        conf = Config(config_dir)
    
    if conf.download_on_run:
        if not os.path.exists(conf.download_dir):
            os.makedirs(conf.download_dir)

        opener = urllib.request.build_opener()
        opener.addheaders = [("User-agent", "Mozilla/5.0")]
        urllib.request.install_opener(opener)
        urllib.request.urlretrieve(DOWNLOAD_URL, conf.download_dir+"board.bin")

    if conf.save_old_files:
        if not conf.save_dir:
            conf.save_dir = os.getcwd()
            e = input("Save dir not specified, saving to current working dir {}. Press enter to continue")

        if not os.path.exists(conf.save_dir):
            os.makedirs(conf.save_dir)

        shutil.copyfile(HW21, conf.save_dir+"hw21-board.bin")
        shutil.copyfile(HW3, conf.save_dir+"hw3-board.bin")
    
    for f in (HW21, HW3):
        e = input("Copying {} to {}, press enter to continue...".format(conf.download_dir+"board.bin", f))
        if not os.path.exists(conf.download_dir+"board.bin"):
            exit("Could not locate board.bin file in {}".format(conf.download_dir))

        shutil.copyfile(conf.download_dir+"board.bin", f)

if __name__ == "__main__":
    main()