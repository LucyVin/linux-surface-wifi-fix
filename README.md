# Linux Surface Wifi Fix
This python pip-dependency-less script will download the appropriate `board.bin` files from killernetworking.com and install them in place of the existing ath10k firmware files from linux-firmware on debian-based systems. Optional command line options or a config file can be set as shown below.

## !! NOTE !! This is designed for debian-based systems. Arch, Fedora, and other distros may have similar fixes, but you will likely need to make changes to `lib/cli.py`

## Installation
1. Clone this repo `git clone https://gitlab.com/lucyvin/linux-surface-wifi-fix.git`
2. cd into the repo and run `pip install --user .`

## NOTE: command-line/config path evaluation requires full paths as this script must be run as root to work

## Command Line
`download-on-run`: True/False, if False, will use the `download-dir` value or default download location (`/tmp/linux-surface-wifi-fix/`) to look for the board.bin file.

`download-dir`: String, either the location to download the file to, or where the file has been downloaded to.

`save-old-files`: True/False, if True, will copy the old board.bin files to the specified directory in `save-dir` or to the cwd.

`save-dir`: String, location where saved old board.bin files should go.

`config-dir`: String, location of the `linux-surface-wifi-fix.json` config file (defaults to `/user/$HOME/.conf/linux-surface-wifi-fix.json`, will attempt to get non-root user)


## Config File
Config values are configured the same as above, but use underscores instead of dashes. An example is below:
```
# /home/foobar/.config/linux-surface-wifi.json
{
    "download_on_run":true,
    "save_old_files":true,
    "save_dir":"/home/foobar/board-old/"
}
```
