from setuptools import setup, find_packages

setup(
    name="linux-surface-wifi-fix",
    version="0.1",
    author="Lucy Vin",
    author_email="lucy@lucyvin.com",
    description=("A lightweight, simple command-line script to install ath10k board.bin files on surface linux"),
    license="MIT",
    keywords="ath10k installer surface-linux",
    url="https://gitlab.com/lucyvin/linux-surface-wifi-fix",
    entry_points={
        "console_scripts":["linux-surface-wifi-fix=lib.cli:main"]
        }
)